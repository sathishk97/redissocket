package com.divum.redissocket

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import redis.clients.jedis.Jedis
import redis.clients.jedis.JedisPubSub

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var thread = Thread {
            val jSubscriber = Jedis("14.192.17.52", 6379)


            jSubscriber.subscribe(object : JedisPubSub() {
                override fun onMessage(channel: String, message: String) {
                    // handle message
                    Log.e("redis", "$message ch : $channel")

                }
            }, "subscription_id1", "subscription_id2", "subscription_id3")
        }
        thread.start()
    }

}